package database

import (
	"fmt"
	"gorm-postgres/models"
	"log"
	"os"
	"time"

	"strconv"

	"github.com/redis/go-redis/v9"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func convertToDuration(input string) (time.Duration, error) {
	duration, err := time.ParseDuration(input)
	if err != nil {
		return 0, err
	}
	return duration, nil
}

type Dbinstance struct {
	Db     *gorm.DB
	Rds    *redis.Client
	RdsTTL time.Duration
}

var DB Dbinstance

//var ctx = context.Background()

func ConnectDb() {

	DB_HOST := os.Getenv("DB_HOST")
	DB_PORT := os.Getenv("DB_PORT")
	DB_NAME := os.Getenv("DB_NAME")
	DB_USER := os.Getenv("DB_USER")
	DB_PASSWORD := os.Getenv("DB_PASSWORD")

	REDIS_HOST := os.Getenv("REDIS_HOST")
	REDIS_PORT := os.Getenv("REDIS_PORT")

	REDIS_PASSWORD := os.Getenv("REDIS_PASSWORD")

	REDIS_TTL, err := convertToDuration(os.Getenv("REDIS_TTL"))
	if err != nil {
		fmt.Println("Ошибка конвертации")
	}

	REDIS_DB_INDEX, err := strconv.Atoi(os.Getenv("REDIS_DB_INDEX"))
	if err != nil {
		fmt.Println("Ошибка конвертации")
	}

	dsn := fmt.Sprintf("host=%s user=%s password=%s database=%s port=%s", DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})

	if err != nil {
		log.Fatal("Failed to connect to database. \n", err)
	}

	log.Println("connected")
	//db.Logger = logger.Default.LogMode(logger.Info)
	log.Println("running migrations")

	err = db.AutoMigrate(
		&models.Company{},
		&models.User{},
		&models.AccessType{},
		&models.SellPoint{},
		&models.QueredCompany{},
		&models.SellHistory{},
		&models.Item{},
	)

	rdb := redis.NewClient(&redis.Options{
		Addr:     REDIS_HOST + ":" + REDIS_PORT,
		Password: REDIS_PASSWORD,
		DB:       REDIS_DB_INDEX,
	})

	if err != nil {
		log.Println(err)
	}

	DB = Dbinstance{
		Db:     db,
		Rds:    rdb,
		RdsTTL: REDIS_TTL,
	}
}
