package utils

import (
	"context"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"gorm-postgres/database"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
)

func GetUserRole(c *fiber.Ctx) string {
	authHeader := c.Get("Authorization")
	if authHeader == "" || !strings.HasPrefix(authHeader, "Basic ") {
		return "-1"
	}
	authString := strings.TrimPrefix(authHeader, "Basic ")

	decodedAuthBytes, err := base64.StdEncoding.DecodeString(authString)
	if err != nil {
		return "-1"
	}
	decodedAuthString := string(decodedAuthBytes)
	credentials := strings.Split(decodedAuthString, ":")
	if len(credentials) != 2 {
		return "-1"
	}
	username := credentials[0]
	ctx := context.Background()
	userRole, err := database.DB.Rds.Get(ctx, username+"Role").Result()
	if err != nil {
		return "-1"
	}
	return userRole
}

// this for encrypt/decrypt of user password
var bytes = []byte{35, 46, 57, 24, 85, 35, 24, 74, 87, 35, 88, 98, 66, 32, 14, 05}

func Encode(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}
func Decode(s string) []byte {
	data, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		panic(err)
	}
	return data
}

func Encrypt(text, key string) (string, error) {
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", err
	}
	plainText := []byte(text)
	cfb := cipher.NewCFBEncrypter(block, bytes)
	cipherText := make([]byte, len(plainText))
	cfb.XORKeyStream(cipherText, plainText)
	return Encode(cipherText), nil
}

func Decrypt(text, key string) (string, error) {
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", err
	}
	cipherText := Decode(text)
	cfb := cipher.NewCFBDecrypter(block, bytes)
	plainText := make([]byte, len(cipherText))
	cfb.XORKeyStream(plainText, cipherText)
	return string(plainText), nil

}

// constants for random secret password generator with encrypt/decrypt
const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!#;%^?1234567890"
const (
	letterIdxBits = 6                    // 6 битов для представления индекса символа
	letterIdxMask = 1<<letterIdxBits - 1 // Все единицы, до letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // Количество индексов, которое помещается в 63 бита
)

func GenerateRandomString(n int) string {
	rand.Seed(time.Now().UTC().UnixNano())

	b := make([]byte, n)
	for i, cache, remain := n-1, rand.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

func ValidateIIN(iin string) bool {
	// Проверяем контрольную сумму ИИН
	sum := 0
	multipliers := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}
	for i, char := range iin[:10] {
		digit, _ := strconv.Atoi(string(char))
		sum += digit * multipliers[i]
	}
	checksum := sum % 11
	if checksum == 10 {
		// Если контрольная сумма равна 10, то делаем дополнительные вычисления
		sum = 0
		multipliers = []int{3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2}
		for i, char := range iin[:11] {
			digit, _ := strconv.Atoi(string(char))
			sum += digit * multipliers[i]
		}
		checksum = sum % 11
		if checksum == 10 {
			checksum = 0
		}
	}
	lastDigit, _ := strconv.Atoi(string(iin[11]))
	if checksum == lastDigit {
		return true
	} else {
		return false
	}
}
