package consts

import (
	"os"
)

type Const struct {
	EncryptKey string
}

var Constants Const

func GetConstants() {
	Constants.EncryptKey = os.Getenv("ENCRYPT_KEY")
}
