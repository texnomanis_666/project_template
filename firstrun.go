package main

import (
	"fmt"
	"gorm-postgres/consts"
	"gorm-postgres/database"
	"gorm-postgres/routes"
	"log"

	"os"

	"github.com/gofiber/fiber/v2"

	"github.com/joho/godotenv"
)

func main() {
	if err := godotenv.Load(); err != nil {
		fmt.Println("Error loading .env file")
	}
	port := os.Getenv("APPLICATION_PORT")

	database.ConnectDb()
	consts.GetConstants()
	app := fiber.New()

	setUpRoutes(app)

	app.Use(handleNotFound)

	log.Fatal(app.Listen(":" + port))

}

func setUpRoutes(app *fiber.App) {
	app.Get("/firstrun/", routes.FirstRun)
}

func handleNotFound(c *fiber.Ctx) error {
	return c.SendStatus(404)
}
