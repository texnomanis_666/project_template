package main

import (
	"context"
	"fmt"
	"gorm-postgres/consts"
	"gorm-postgres/database"
	"gorm-postgres/models"
	"gorm-postgres/routes"
	"gorm-postgres/utils"
	"log"
	"os"

	//"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/basicauth"

	"github.com/joho/godotenv"
)

func CheckPassword(openPass, encryptPass, key string) bool {
	s, err := utils.Decrypt(encryptPass, key)

	if err != nil {
		return false
	}

	if s == openPass {
		return true
	} else {
		return false
	}

}

func main() {
	if err := godotenv.Load(); err != nil {
		fmt.Println("Error loading .env file")
	}

	port := os.Getenv("APPLICATION_PORT")

	database.ConnectDb()
	consts.GetConstants()
	app := fiber.New()
	//	adminService := fiber.New()

	app.Use(basicauth.New(basicauth.Config{

		Realm: "Forbidden",
		Authorizer: func(user, pass string) bool {

			var userName, userPass string
			var userRole int
			var err error
			// лезем в редис
			// проверяем ключи в редисе
			ctx := context.Background()

			userName, err = database.DB.Rds.Get(ctx, user+"Key").Result()
			if err != nil {

				// если нет то проверяем их наличие в основной базе и копируем в редис
				needUser := &models.User{}
				errGorm := database.DB.Db.Where("login = ?", user).First(&needUser)
				if errGorm != nil {
					userName = needUser.Login
					userPass = needUser.Password
					userRole = needUser.Role

					err = database.DB.Rds.Set(ctx, user+"Key", userName, 0).Err()
					if err != nil {
						return false
					}

					err = database.DB.Rds.Expire(ctx, user+"Key", database.DB.RdsTTL).Err()
					if err != nil {
						return false
					}

					err = database.DB.Rds.Set(ctx, user+"Pass", userPass, 0).Err()
					if err != nil {
						return false
					}

					err = database.DB.Rds.Expire(ctx, user+"Pass", database.DB.RdsTTL).Err()
					if err != nil {
						return false
					}

					err = database.DB.Rds.Set(ctx, user+"Role", userRole, 0).Err()
					if err != nil {
						return false
					}

					err = database.DB.Rds.Expire(ctx, user+"Role", database.DB.RdsTTL).Err()
					if err != nil {
						return false
					}

				} else {
					return false
				}
			}

			userPass, err = database.DB.Rds.Get(ctx, user+"Pass").Result()
			if err != nil {
				return false
			}

			//userRoleStr, err := database.DB.Rds.Get(ctx, user+"Role").Result()
			//if err != nil {
			//	return false
			//}
			//
			//userRole, err = strconv.Atoi(userRoleStr)
			//if err != nil {
			//	return false
			//}

			return CheckPassword(pass, userPass, consts.Constants.EncryptKey)

		},
		Unauthorized: func(c *fiber.Ctx) error {
			return c.SendFile("./unauthorized.html")
		},
		ContextUsername: "_user",
		ContextPassword: "_pass",
	}))

	setUpRoutes(app)
	//	adminSetUpRoutes(adminService)

	app.Use(handleNotFound)
	//	adminService.Use(handleNotFound)

	//	log.Fatal(adminService.Listen(":" + adminPort))
	log.Fatal(app.Listen(":" + port))

}

//func adminSetUpRoutes(app *fiber.App) {
//	app.Get("/firstrun/", routes.FirstRun)
//}

func setUpRoutes(app *fiber.App) {
	app.Get("/company/:bin", routes.GetCompany)
	app.Post("/company/new/", routes.RegCompany)
	app.Post("/company/regrequest/", routes.RegCompanyQuery)
	app.Post("/company/edit/", routes.EditCompany)
	app.Delete("/company/", routes.DeleteCompany)

	app.Get("/user/:login", routes.GetUser)
	app.Post("/user/new/", routes.AddUser)
	app.Post("/user/edit/", routes.EditUser)
	app.Delete("/user/", routes.DeleteUser)

	app.Get("/sellpoint/:id", routes.GetSellPoint)
	app.Post("/sellpoint/new/", routes.AddSellPoint)
	app.Post("/sellpoint/edit/", routes.EditSellPoint)
	app.Delete("/sellpoint/", routes.DeleteSellPoint)

	app.Get("/:company_id/get_history/:start_date/:end_date/", routes.GetHistory)

	app.Get("/firstrun/", routes.FirstRun)
}

func handleNotFound(c *fiber.Ctx) error {
	return c.SendStatus(404)
}
