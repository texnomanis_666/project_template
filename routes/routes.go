package routes

import (
	"gorm-postgres/consts"
	"gorm-postgres/database"
	"gorm-postgres/models"
	"gorm-postgres/utils"

	"github.com/gofiber/fiber/v2"
)

// отправить данные компании мэнэнджеру
func RegCompanyQuery(c *fiber.Ctx) error {
	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {
		var company models.QueredCompany
		if err := c.BodyParser(&company); err != nil {
			return c.SendString("{status:false}")
		}
		database.DB.Db.Create(&company)
		return c.SendString("{status:ok}")
	} else {
		return c.SendString("{status:accesDenied}")
	}

}

/*
механизм регистрации компании
1)данные компании попадают во временную таблицу
2)менеджер руками смотрит эти данные и принимает решение о регистрации
3) при регистрации этот менеджер становиться менеджером этой компании и его ИД привязывается к компании
4) в случае если компании "не прошла" её данные остаются во временной таблице
*/

// эта функция регистрирует компанию в основной базе
// добавляется новый ИД и в отправляемом json-e добавляется ИД менеджера
func RegCompany(c *fiber.Ctx) error {
	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {
		var company models.Company
		var queredCompany models.QueredCompany
		if err := c.BodyParser(&company); err != nil {
			return c.SendString("{status:false}")
		}
		database.DB.Db.Create(&company)
		database.DB.Db.Where("bin = ?", &company.Bin).First(&queredCompany)
		database.DB.Db.Delete(&queredCompany)
		return c.SendString("{status:ok}")
	} else {
		return c.SendString("{status:accesDenied}")
	}

}

func EditCompany(c *fiber.Ctx) error {
	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {
		var company models.Company
		if err := c.BodyParser(&company); err != nil {
			return c.SendString("{status:false}")
		}
		database.DB.Db.Save(&company)
		return c.SendString("{status:ok}")
	} else {
		return c.SendString("{status:accesDenied}")
	}

}

func DeleteCompany(c *fiber.Ctx) error {

	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {
		var company models.Company
		if err := c.BodyParser(&company); err != nil {
			return c.SendString("{status:false}")
		}
		database.DB.Db.Delete(&company)
		return c.SendString("{status:ok}")
	} else {
		return c.SendString("{status:accesDenied}")
	}

}

func GetCompany(c *fiber.Ctx) error {
	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {

		company := &models.Company{}
		bin := c.Params("bin")
		err := database.DB.Db.Where("bin = ?", bin).First(&company)
		if err != nil {
			return c.JSON(&company)
		} else {
			return c.SendString("{status:companyNotFound}")
		}
	} else {
		return c.SendString("{status:accesDenied}")
	}

}

func AddUser(c *fiber.Ctx) error {

	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {

		var user models.User
		if err := c.BodyParser(&user); err != nil {
			return c.SendString("{status:false}")
		}

		var existingUser models.User
		result := database.DB.Db.Where("login = ?", user.Login).First(&existingUser)
		if result.Error != nil {
			if result.Error != nil {
				// Добавление записи, если пользователя с таким именем нет в базе данных

				user.Password, _ = utils.Encrypt(user.Password, consts.Constants.EncryptKey)

				database.DB.Db.Create(&user)
				return c.SendString("{status:true}")
			}
			return c.SendString("{status:false}")
		}

		return c.SendString("{status:exists}")

	} else {
		return c.SendString("{status:accesDenied}")
	}

}

func GetUser(c *fiber.Ctx) error {
	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {

		user := &models.User{}
		login := c.Params("login")
		err := database.DB.Db.Where("login = ?", login).First(&user)
		if err != nil {
			user.Password = ""
			return c.JSON(&user)
		} else {
			return c.SendString("{status:userNotFound}")
		}
	} else {
		return c.SendString("{status:accesDenied}")
	}
}

func EditUser(c *fiber.Ctx) error {
	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {

		var user models.User
		if err := c.BodyParser(&user); err != nil {
			return c.SendString("{status:false}")
		}
		database.DB.Db.Save(&user)
		return c.SendString("{status:ok}")
	} else {
		return c.SendString("{status:accesDenied}")
	}
}

func DeleteUser(c *fiber.Ctx) error {

	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {

		var user models.User
		if err := c.BodyParser(&user); err != nil {
			return c.SendString("{status:false}")
		}
		database.DB.Db.Delete(&user)
		return c.SendString("{status:ok}")
	} else {
		return c.SendString("{status:accesDenied}")
	}
}

func GetSellPoint(c *fiber.Ctx) error {
	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {
		sellPoint := &models.SellPoint{}
		id := c.Params("id")
		err := database.DB.Db.Where("id = ?", id).First(&sellPoint)
		if err != nil {
			return c.JSON(&sellPoint)
		} else {
			return c.SendString("{status:userNotFound}")
		}
	} else {
		return c.SendString("{status:accesDenied}")
	}

}

func AddSellPoint(c *fiber.Ctx) error {

	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {
		var sellPoint models.SellPoint
		if err := c.BodyParser(&sellPoint); err != nil {
			return c.SendString("{status:false}")
		}
		database.DB.Db.Create(&sellPoint)
		return c.SendString("{status:ok}")
	} else {
		return c.SendString("{status:accesDenied}")
	}

}

func EditSellPoint(c *fiber.Ctx) error {

	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {
		var sellPoint models.SellPoint
		if err := c.BodyParser(&sellPoint); err != nil {
			return c.SendString("{status:false}")
		}
		database.DB.Db.Save(&sellPoint)
		return c.SendString("{status:ok}")
	} else {
		return c.SendString("{status:accesDenied}")
	}

}

func DeleteSellPoint(c *fiber.Ctx) error {
	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5") {

		var sellPoint models.SellPoint
		if err := c.BodyParser(&sellPoint); err != nil {
			return c.SendString("{status:false}")
		}
		database.DB.Db.Delete(&sellPoint)
		return c.SendString("{status:ok}")
	} else {
		return c.SendString("{status:accesDenied}")
	}

}

func GetHistory(c *fiber.Ctx) error {
	role := utils.GetUserRole(c)
	if role != "-1" && (role == "1" || role == "5" || role == "3") {

		var sellPoints []models.SellHistory
		companyId := c.Params("company_id")
		startTime := c.Params("start_time")
		endTime := c.Params("end_time")

		database.DB.Db.Where("companyId = ? ", companyId).Where("sellTime BETWEEN ? AND ?", startTime, endTime).Find(&sellPoints)
		return c.SendString("{status:ok}")
	} else {
		return c.SendString("{status:accesDenied}")
	}

}

// функция которая проверяет наличие первого пользователя (админа)
// если админа нет то он создаётся если
// если существувет другой админ (любой пользователь с той же ролью) то новый админ не создаётся
func FirstRun(c *fiber.Ctx) error {
	user := &models.User{}
	// 5 номер роли админа
	err := database.DB.Db.Where("role = ?", 5).First(&user)

	if err != nil {
		user.Login = "admin"
		user.Password, _ = utils.Encrypt("adminadminadmin", consts.Constants.EncryptKey)
		user.Role = 5

		//if err != nil {
		//	return c.SendString("{status:encryptError}")
		//}

		err := database.DB.Db.Create(&user)
		if err != nil {
			return c.SendString("{status:adminNotFoundAndCreated}")
		} else {
			return c.SendString("{status:adminNotFoundAndNotCreated}")
		}
	} else {
		return c.SendString("{status:adminFound}")
	}

}
