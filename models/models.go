package models

import (
	"time"

	"gorm.io/gorm"
)

type Company struct {
	gorm.Model

	Id           int    `json:"id",gorm:"primaryKey"`
	Name         string `json:"name"`
	Bin          string `json:"bin",gorm:"unique"`
	Address      string `json:"address"`
	Type         string `json:"type"` // ао тоо ип
	Phone        string `json:"phone"`
	DirectorName string `json:"directorName"`
	ManagerId    int    `json:"managerId"`
	Email        string `json:"email"`
}

type QueredCompany struct {
	gorm.Model
	Id           int    `json:"id",gorm:"primaryKey"`
	Name         string `json:"name"`
	Bin          string `json:"bin",gorm:"unique"`
	Address      string `json:"address"`
	Type         string `json:"type"` // ао тоо ип
	Phone        string `json:"phone"`
	DirectorName string `json:"directorName"`
	ManagerId    int    `json:"managerId"`
	Email        string `json:"email"`
}

type User struct {
	gorm.Model
	Id        int    `json:"id",gorm:"primaryKey"`
	Name      string `json:"name"`
	Role      int    `json:"role"`
	CompanyId int    `json:"companyId"`
	Login     string `json:"login",gorm:"primaryKey,unique"`
	Password  string `json:"password"`
	email     string `json:"email"`
}

type SellPoint struct {
	gorm.Model
	Id           int    `json:"id",gorm:"primaryKey"`
	address      string `json:"address"`
	CompanyId    int    `json:"companyId"`
	SerialNumber string `json:"serialNumber"`
}

type SellHistory struct {
	gorm.Model
	Id          int       `json:"id",gorm:"primaryKey"`
	SellPointId int       `json:"sellPointId"`
	SellTime    time.Time `json:"sellTime", gorm:"type:timestamp"`
	ItemId      int       `json:"itemId"`
	CompanyId   int       `json:"companyId"`
}

type Item struct {
	gorm.Model
	Id          int     `json:"id",gorm:"primaryKey"`
	Description string  `json:"description"`
	Baracode    int     `json:"baracode"`
	Price       float32 `json:"price"`
}

type AccessType struct {
	gorm.Model
	Id          int    `json:"id",gorm:"primaryKey"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

/*
список ролей
0 - не указана (никаких прав, только регистрация)
1 - директор компании
2 - бухгалтер
3 - кассир
4 - терминал ????надо ли ?
5 - админ системы (верховный админ)
6 - личный менеджер (менеджер толема)
7 - менеджер компании

*/
